//! Logic gate LUTs
use crate::Data;
use crate::Descriptor;

/// Evaluate (A | B) and branch
pub struct Or {
    /// Fill scratch with address of a false evaluation
    pub spray: Descriptor,

    /// Copy size argument into the A descriptor
    pub length_a: Descriptor,
    /// Address of size argument to copy
    pub length_a_ptr: Data,

    /// Overwrite scratch depending on the size argument
    pub a: Descriptor,

    /// Copy size argument into the B descriptor
    pub length_b: Descriptor,
    /// Address of size argument to copy
    pub length_b_ptr: Data,

    /// Overwrite scratch depending on the size argument
    pub b: Descriptor,

    /// Load the result address into the jump descriptor
    pub load: Descriptor,
    /// Jump to the address returned by the logical expression
    pub jmp: Descriptor,

    /// Address of the branch to take upon a true evaluation
    pub true_ptr: Data,

    /// Address of the branch to take upon a false evaluation
    pub false_ptr: Data,
}

impl Or {
    /// Constructs an OR operation
    ///
    /// Each descriptor writes to the same space, if either transfer overwrites the
    /// memory with the true location, it jumps to the address defined by the descriptor value.
    /// If neither transfer overwrites the memory, it jumps to the false descriptor.
    pub fn new(
        scratch: usize,

        length_a_loc: usize,
        length_a_ptr: usize,

        a_loc: usize,

        length_b_loc: usize,
        length_b_ptr: usize,

        b_loc: usize,

        load_loc: usize,
        load_ptr: usize,

        jmp_loc: usize,

        true_ptr: usize,
        false_ptr: usize,

    ) -> Or {
        // Spray the area with the false jump
        let spray = Descriptor {
            source: false_ptr,
            dest: scratch,
            len: 2,
            next: Some(length_a_loc),
            increment_source: false,
            increment_dest: true,
        };

        // Setup the input argument for a
        let length_a = ReadLength::new(length_a_ptr, a_loc, a_loc);
        let length_a = length_a.length;

        // Attempt to overwrite the false jump with size of a
        let a = Descriptor {
            source: true_ptr,
            dest: scratch,
            len: 0,
            next: Some(length_b_loc),
            increment_source: false,
            increment_dest: true,
        };

        // Setup the input argument for b
        let length_b = ReadLength::new(length_b_ptr, b_loc, b_loc);
        let length_b = length_b.length;

        // Attempt to overwrite the false jump with size of b
        let b = Descriptor {
            source: true_ptr,
            dest: scratch,
            len: 0,
            next: Some(load_loc),
            increment_source: false,
            increment_dest: true,
        };

        // Jump to the resulting address stored in the scratchspace
        let read = ReadJump::new(scratch + 1, load_ptr, jmp_loc);
        let load = read.load;
        let jmp = read.jmp;

        Self {
            spray,
            length_a,
            length_a_ptr: Data::Pointer(length_a_ptr),
            a,
            length_b,
            length_b_ptr: Data::Pointer(length_b_ptr),
            b,
            load,
            jmp,
            true_ptr: Data::Pointer(true_ptr),
            false_ptr: Data::Pointer(false_ptr),
        }
    }
}

/// Construct a descriptor for loading scratch data into another descriptor's size field.
pub struct ReadLength {
    /// Copies the size into the target descriptor.
    pub length: Descriptor,
}

impl ReadLength {
    pub fn new(length_loc: usize, target_loc: usize, next: usize) -> Self {
        // Copy address into jump descriptor
        Self {
            length: Descriptor {
                source: length_loc,
                dest: target_loc,
                len: 1,
                next: Some(next),
                increment_source: false,
                increment_dest: false,
            }
        }
    }
}

/// Construct load and jump descriptors for evaluating a result
pub struct ReadJump {
    /// Copies scratch memory into the jump descriptor's next field and then jumps to `jmp`.
    pub load: Descriptor,

    /// Performs a dummy copy (since all run descriptors must copy at least one item) and then jumps to the loaded address.
    pub jmp: Descriptor,
}

impl ReadJump {
    /// Setup a load-jump instruction
    pub fn new(scratch: usize, load_ptr: usize, jmp_loc: usize) -> Self {
        // Copy address into jump descriptor
        let load = Descriptor {
            source: scratch,
            dest: jmp_loc,
            len: 1,
            next: Some(jmp_loc),
            increment_source: false,
            increment_dest: false,
        };

        // Perform a NOP write and jump to loaded address
        let jmp = Descriptor {
            source: load_ptr,
            dest: load_ptr,
            len: 1,
            next: None,
            increment_source: false,
            increment_dest: false,
        };

        Self {
            load,
            jmp
        }
    }
}

/// Evaluate (A & B) and branch
///
/// The order of the members should roughly align with the order of the described operations.
pub struct And {
    /// Prepares scratch with the address of the false branch.
    pub spray: Descriptor,

    /// Overwrite first half of scratch with the address of the second comparison.
    /// If A is true, then B will also have to be evaluated.
    pub a: Descriptor,

    /// Load the value of A into the first comparison.
    pub length_a: Descriptor,
    /// Address of size argument to copy
    pub length_a_ptr: Data,

    /// Pointer to the A descriptor
    pub a_ptr: Data,

    /// Copy the result of the first comparison into a jump descriptor.
    pub load_a: Descriptor,
    /// Address of the load descriptor for the first comparison.
    pub load_a_ptr: Data,

    /// Jump to the result of the first comparison.
    pub jmp_a: Descriptor,
    /// Address of the jump descriptor for the first comparison.
    pub jmp_a_ptr: Data,

    /// Overwrite scratch with the address of the true branch.
    /// If B is true, then the resulting jump address will be overwritten.
    pub b: Descriptor,
    /// Pointer to the B descriptor
    pub b_ptr: Data,

    /// Copy the result of the second comparison into a jump descriptor.
    pub load_b: Descriptor,
    pub load_b_ptr: Data,

    /// Jump to the result of the second comparison.
    pub jmp_b: Descriptor,
    /// Address of the load descriptor for the second comparison.
    pub jmp_b_ptr: Data,

    /// Load the value of B into the second comparison.
    pub length_b: Descriptor,
    /// Address of size argument to copy
    pub length_b_ptr: Data,

    /// Address of the branch to take upon a true evaluation
    pub true_ptr: Data,

    /// Address of the branch to take upon a false evaluation
    pub false_ptr: Data,
}

impl And {
    /// Constructs an AND operation
    ///
    /// Spray a logic area for two two-word transfers with the address of the false branch.
    /// Transfer a writes the address of the b descriptor and then jumps to the second
    /// written address--if it was written--otherwise, if jumps to the false branch.
    /// The second logic area is then sprayed with the true branch by b, after which
    /// b jumps using the last address in the second logic area.
    pub fn new(
        branch: usize,

        length_a_loc: usize,
        length_a_ptr: usize,
        a_loc: usize,
        a_ptr: usize,

        length_b_loc: usize,
        length_b_ptr: usize,
        b_loc: usize,
        b_ptr: usize,

        load_a_ptr: usize,
        load_a_loc: usize,
        jmp_a_ptr: usize,
        jmp_a_loc: usize,

        load_b_ptr: usize,
        load_b_loc: usize,
        jmp_b_ptr: usize,
        jmp_b_loc: usize,

        true_ptr: usize,
        true_loc: usize,
        false_ptr: usize,
        false_loc: usize,
    ) -> Self {
        // Spray the area with the address of the false jump
        let spray = Descriptor {
            source: false_ptr,
            dest: branch,
            len: 4,
            next: Some(length_a_loc),
            increment_source: false,
            increment_dest: true,
        };

        let length_a = ReadLength::new(length_a_ptr, a_loc, a_loc);

        // Write address of the b conditional
        let a = Descriptor {
            source: length_b_loc,
            dest: branch,
            len: 0, // if a { 2 } else { 1 },
            next: Some(load_a_loc),
            increment_source: false,
            increment_dest: true,
        };

        let read_a = ReadJump::new(branch + 1, load_a_ptr, jmp_a_loc);
        let load_a = read_a.load;
        let jmp_a = read_a.jmp;

        let length_b = ReadLength::new(length_b_ptr, b_loc, b_loc);

        // Setup return address for second comparison
        let b = Descriptor {
            source: true_ptr,
            dest: branch + 2,
            len: 0, // if b { 2 } else { 1 },
            next: Some(load_b_loc),
            increment_source: false,
            increment_dest: true,
        };

        let read_b = ReadJump::new(branch + 3, load_b_ptr, jmp_b_loc);
        let load_b = read_b.load;
        let jmp_b = read_b.jmp;

        Self {
            spray,

            a,
            a_ptr: Data::Pointer(a_loc),
            load_a,
            load_a_ptr: Data::Pointer(load_a_ptr),
            length_a: length_a.length,
            length_a_ptr: Data::Pointer(length_a_ptr),
            jmp_a,
            jmp_a_ptr: Data::Pointer(jmp_a_ptr),

            b,
            b_ptr: Data::Pointer(b_loc),
            load_b,
            load_b_ptr: Data::Pointer(load_b_ptr),
            length_b: length_b.length,
            length_b_ptr: Data::Pointer(length_b_ptr),
            jmp_b,
            jmp_b_ptr: Data::Pointer(jmp_b_ptr),

            true_ptr: Data::Pointer(true_loc),
            false_ptr: Data::Pointer(false_loc),
        }
    }
}
