//! Implements linked symbols
use crate::*;

/// A table of mapped symbols, similar to the output of a linker script
///
/// TODO: this should probably be implemented using a HashMap or Set
///       but right now I just want something simple and debuggable
///       so an array it is
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SymbolTable<const SIZE: usize> {
    /// Memory layout
    table: [Option<&'static str>; SIZE],
}

impl<const SIZE: usize> From<[Option<&'static str>; SIZE]> for SymbolTable<SIZE> {
    fn from(table: [Option<&'static str>; SIZE]) -> Self {
        Self {
            table,
        }
    }
}

impl<const SIZE: usize> SymbolTable<SIZE> {
    /// Initialize a new symbol table
    pub fn new() -> Self {
        Self {
            table: [None; SIZE],
        }
    }

    /// Check if a symbol already exists within the table
    fn check_exists(&self, symbol: &'static str) -> Result<(), VmError> {
        for s in self.table {
            if let Some(sym) = s {
                if sym == symbol {
                    return Err(VmError::DuplicateSymbol(sym));
                }
            }
        }

        Ok(())
    }

    /// Check that all defined symbols are initialized
    pub fn check_init(&self, mem: &Memory) -> Result<(), VmError> {
        for (i, s) in self.table.iter().enumerate() {
            if let Some(_) = s {
                if let Word::Uninitialized = mem.0[i] {
                    return Err(VmError::ExpectedInit(i));
                }
            }
        }

        Ok(())
    }

    /// Assign a symbol to the table
    pub fn assign(&mut self, index: usize, symbol: &'static str) -> Result<(), VmError> {
        if index < SIZE {
            self.check_exists(symbol)?;
            self.table[index] = Some(symbol);
            Ok(())
        } else {
            Err(VmError::IndexOutOfBounds(index))
        }
    }

    /// Retrieve an index from the table by its symbol
    pub fn get(&self, symbol: &'static str) -> Result<usize, VmError> {
        for (i, s) in self.table.iter().enumerate() {
            if let Some(sym) = s {
                if sym ==&symbol {
                    return Ok(i);
                }
            }
        }

        Err(VmError::InvalidSymbol(symbol))
    }

    /// Retrieve a symbol from the table by its index
    pub fn get_index(&self, index: usize) -> Result<&'static str, VmError> {
        if index < SIZE {
            if let Some(sym) = self.table[index] {
                Ok(sym)
            } else {
                Err(VmError::InvalidIndex(index))
            }
        } else {
            Err(VmError::IndexOutOfBounds(index))
        }
    }
}

#[allow(unused)]
mod test {
    use crate::VmError;

    /// Predefined symbols to use in test
    const SYMBOL_TABLE: [Option<&'static str>; 4] = [Some("a"), Some("b"), None, None];

    #[test]
    fn test_get() {
        let mut table: crate::SymbolTable<4> = SYMBOL_TABLE.into();
        table.assign(2, "c").unwrap();
        assert!(table.get_index(0).unwrap() == "a");
        assert!(table.get("a").unwrap() == 0);
        assert!(table.get_index(1).unwrap() == "b");
        assert!(table.get("b").unwrap() == 1);
        assert!(table.get_index(2).unwrap() == "c");
        assert!(table.get("c").unwrap() == 2);
        assert!(table.get_index(3) == Err(VmError::InvalidIndex));
        assert!(table.get("d") == Err(VmError::InvalidSymbol));
    }
}
