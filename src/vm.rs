//! Virtual machine implementation
use core::convert::{TryFrom, TryInto};

/// Virtual machine execution errors
#[derive(Debug, PartialEq)]
pub enum VmError {
    /// Loaded Word is not a descriptor
    InvalidDescriptor(Word),

    /// An access was made to the descriptor currently in use
    InvalidAccess,

    /// Tried to write protected memory
    ProtectedWrite,

    /// Request index was out of bounds
    IndexOutOfBounds(usize),

    /// Index was not populated
    InvalidIndex(usize),

    /// Symbol did not exist in table
    InvalidSymbol(&'static str),

    /// Memory pointed to by symbol was not initialized
    ExpectedInit(usize),

    /// Memory at symbol did not match its expected contents
    UnexpectedWord(Word),

    /// A stored object in the memory model exists in multiple places
    DuplicateSymbol(&'static str),
}

/// In-memory representation of a DMA transfer
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Descriptor {
    /// Start of memory to copy from
    pub source: usize,

    /// Start of memory to copy into
    pub dest: usize,

    /// Length of data to copy
    pub len: usize,

    /// Index of next Descriptor
    pub next: Option<usize>,

    /// Move source memory forward once every transfer
    pub increment_source: bool,

    /// Move destination memory forward once every transfer
    pub increment_dest: bool,
}

impl From<Descriptor> for Word {
    fn from(d: Descriptor) -> Self {
        Self::Descriptor(d)
    }
}

/// Any mapped object that can generate data
pub trait Input {
    /// Get any type of input from a source
    fn get(&mut self, type_: Word) -> Word {
        type_
    }
}

/// A peripheral that will generate a wrapping and ascending count
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Counter {
    /// Current counter value
    pub count: usize,

    /// Treat output as a length parameter
    length: bool,
}

impl Counter {
    pub fn new(count: usize, length: bool) -> Self {
        Self {
            count,
            length,
        }
    }
}

impl Input for Counter {
    fn get(&mut self, _: Word) -> Word {
        let output = self.count;

        self.count = self.count.wrapping_add(1);

        if self.length {
            Word::Data(Data::Length(output))
        } else {
            Word::Data(Data::Byte(output as u8))
        }
    }
}

/// Memory with defined content
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Data {
    /// An address to some memory
    Pointer(usize),
    /// A transfer length
    Length(usize),
    /// A small integer
    Byte(u8),
    /// Nondescript user data
    Bulk,
}

impl Default for Data {
    fn default() -> Self {
        Data::Bulk
    }
}

impl From<Data> for Word {
    fn from(d: Data) -> Self {
        Self::Data(d)
    }
}


/// An abstract representation of "unit" memory
///
/// One unit memory can contain any amount of data, because there is only limited spatial ordering.
/// This means that a 4-word linked-list descriptor occupies the same amount of space as data or peripherals.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Word {
    /// A piece of memory describing a transaction
    Descriptor(Descriptor),

    /// Mapped memory which acts a generator when copied out of
    Peripheral(Counter),

    /// Memory with known content
    Data(Data),

    /// Memory without known content
    Uninitialized,

    /// Reserved memory for exclusive use by the DMA
    Reserved,

    /// Protected memory, a write to this area generates an exception
    Protected(&'static str),
}

impl Default for Word {
    fn default() -> Self {
        Self::Uninitialized
    }
}

impl TryFrom<&Word> for Descriptor {
    type Error = VmError;

    fn try_from(w: &Word) -> Result<Self, Self::Error> {
        if let Word::Descriptor(d) = w {
            Ok(*d)
        } else {
            Err(Self::Error::InvalidDescriptor(*w))
        }
    }
}

/// A block of contiguous memory
#[derive(Debug, Clone)]
pub struct Memory(pub Vec<Word>);

impl Memory {
    /// Create memory of given length
    pub fn new(len: usize) -> Self {
        let mut v = Vec::new();

        for _ in 0..len {
            v.push(Word::default());
        }

        Self(v)
    }
}

impl core::fmt::Display for Memory {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        for (i, u) in self.0.iter().enumerate() {
            writeln!(f, "{: >4X}: {:X?}", i, u)?;
        }

        Ok(())
    }
}

/// The virtual machine
#[derive(Debug, Clone)]
pub struct Vm {
    /// The instruction memory, contains our current transaction/instruction
    pub current_transfer: Descriptor,

    /// Program counter, contains the last index read to the instruction memory
    current_index: usize,

    /// Current time in instructions processed
    time: usize,

    /// Maximum time allowed to run
    pub limit: usize,
}

impl Vm {
    pub fn try_new(limit: usize, initial_index: usize, ram: &Memory) -> Result<Self, VmError> {
        Ok(Self {
            current_transfer: (&ram.0[initial_index]).try_into()?,
            current_index: initial_index,
            time: 0,
            limit,
        })
    }

    /// Process the current instruction to its end
    ///
    /// Returns a graph of the processed instructions if it ends successuly
    /// or a diagram of all the words it tried to process if it failed.
    pub fn run(&mut self, ram: &mut Memory) -> Result<Vec<(usize, Descriptor)>, (VmError, Vec<(usize, Word)>)> {
        let mut history = Vec::new();

        // Process the linked list
        while match self.process_current(ram) {
            Ok(d) => {
                history.push((self.current_index, Word::Descriptor(self.current_transfer)));

                // Load the next instruction
                if let Some(d) = d {
                    self.current_transfer = d;
                    true
                } else {
                    false
                }
            }
            Err(e) => {
                history.push((self.current_index, Word::Descriptor(self.current_transfer)));
                return Err((e, history));
            }
        } {
            // Step time and check limit
            self.time += 1;
            if self.time > self.limit {
                eprintln!("Time limit reached");
                break;
            }
        }

        // Since the run didn't fail, it is safe to assume that all words are descriptors
        Ok(history
            .iter()
            .map(|(i, w)| (*i, Descriptor::try_from(w).unwrap()))
            .collect())
    }

    /// Apply pointers as parameters in specific situations
    /// TODO: this is basically a ticking bomb, requires restructuring
    ///       to encompass future development
    pub(crate) fn apply(&self, src: &Word, dest: &mut Word) -> Result<bool, VmError> {
        match dest {
            Word::Protected(ref mut d) => return Err(VmError::ProtectedWrite),
            Word::Descriptor(ref mut d) => match src {
                Word::Data(Data::Pointer(p)) => d.next = Some(*p),
                Word::Data(Data::Length(l)) => d.len = *l,
                _ => return Ok(false),
            },
            _ => return Ok(false),
        };

        Ok(true)
    }

    /// Process the current transfer and produce the next transfer to be processed
    fn process_current(&mut self, ram: &mut Memory) -> Result<Option<Descriptor>, VmError> {
        let mut d = self.current_transfer;

        if d.len == 0 {
            Ok(None)
        } else {
            // Process copy, step transfers and decrement length
            if self.current_index == d.dest {
                // Attempted to overwrite the current descriptor
                return Err(VmError::InvalidAccess);
            } else {
                // Handle emulated memory map
                let src = match ram.0[d.source] {
                    Word::Peripheral(ref mut p) => p.get(Word::Data(Data::Bulk)),
                    w => w,
                };

                if !self.apply(&src, &mut ram.0[d.dest])? {
                    ram.0[d.dest] = src;
                }
            }

            if d.increment_source {
                d.source += 1;
            }

            if d.increment_dest {
                d.dest += 1;
            }

            // Decrement the size counter in both the current
            // transfer and the memory representation
            d.len -= 1;
            match ram.0[self.current_index] {
                Word::Descriptor(ref mut desc) => desc.len -= 1,
                _ => (),
            }

            // Last transfer
            if d.len == 0 {
                // All transactions have been made, load next linked list item
                if let Some(i) = d.next {
                    // Another link was specified
                    if let Ok(n) = Descriptor::try_from(&ram.0[i]) {
                        self.current_index = i;
                        Ok(Some(n))
                    } else {
                        Err(VmError::InvalidDescriptor(ram.0[i]))
                    }
                } else {
                    // End of linked list reached
                    return Ok(None);
                }
            } else {
                Ok(Some(d))
            }
        }
    }
}
