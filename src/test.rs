//! Unit tests
#![allow(unused)]
use crate::*;

/// Check that apply properly copies parts of LL-items
#[test]
fn check_apply_next() {
    let copy = Descriptor {
        source: 0x1,
        dest: 0x2,
        len: 1,
        next: None,
        increment_source: false,
        increment_dest: false,
    };

    let target = Descriptor {
        source: 0x0,
        dest: 0x0,
        len: 0,
        next: None,
        increment_source: false,
        increment_dest: false,
    };

    let ptr = Data::Pointer(0xaa);

    let mut ram = Memory::new(3);
    ram.0[0] = copy.into();
    ram.0[1] = ptr.into();
    ram.0[2] = target.into();

    let mut vm = Vm::try_new(2, 0, &ram).unwrap();

    vm.run(&mut ram).unwrap();

    if let Word::Descriptor(d) = ram.0[2] {
        assert!(Data::Pointer(d.next.unwrap()) == ptr);
    } else {
        panic!("expected descriptor");
    }
}

/// Check that apply properly copies parts of LL-items
#[test]
fn check_apply_len() {
    let copy = Descriptor {
        source: 0x1,
        dest: 0x2,
        len: 1,
        next: None,
        increment_source: false,
        increment_dest: false,
    };

    let target = Descriptor {
        source: 0x0,
        dest: 0x0,
        len: 0,
        next: None,
        increment_source: false,
        increment_dest: false,
    };

    let len = Data::Length(0x2);

    let mut ram = Memory::new(3);
    ram.0[0] = copy.into();
    ram.0[1] = len.into();
    ram.0[2] = target.into();

    let mut vm = Vm::try_new(2, 0, &ram).unwrap();

    vm.run(&mut ram).unwrap();

    if let Word::Descriptor(d) = ram.0[2] {
        assert!(Data::Length(d.len) == len);
    } else {
        panic!("expected descriptor");
    }
}


fn setup_and() -> Memory {
    let mut ram = Memory::new(26);

    let and = And::new(
        // Scratchspace
        16,

        // Boolean conditions
        24, 22, 1, 7,
        25, 23, 4, 8,

        // A-descriptor
        9, 2, 10, 3,

        // B-descriptor
        11, 5, 12, 6,

        // Branches
        13, 20, 14, 21,
    );

    // Setup logic gate
    ram.0[0] = and.spray.into();

    ram.0[1] = and.a.into();
    ram.0[2] = and.load_a.into();
    ram.0[3] = and.jmp_a.into();

    ram.0[4] = and.b.into();
    ram.0[5] = and.load_b.into();
    ram.0[6] = and.jmp_b.into();

    // Setup pointer table
    ram.0[7] = and.a_ptr.into();
    ram.0[8] = and.b_ptr.into();

    ram.0[9] = and.load_a_ptr.into();
    ram.0[10] = and.jmp_a_ptr.into();
    ram.0[11] = and.load_b_ptr.into();
    ram.0[12] = and.jmp_b_ptr.into();

    ram.0[13] = and.true_ptr.into();
    ram.0[14] = and.false_ptr.into();

    // Logic scratchpad
    ram.0[16] = Word::Reserved;
    ram.0[17] = Word::Reserved;
    ram.0[18] = Word::Reserved;
    ram.0[19] = Word::Reserved;

    // Setup branch targets
    ram.0[20] = Word::Protected("true");
    ram.0[21] = Word::Protected("false");

    // Constant true and false values
    ram.0[22] = Word::Data(Data::Length(1)); // 2 = true, 1 = false
    ram.0[23] = Word::Data(Data::Length(1)); // as above

    ram.0[24] = and.length_a.into();
    ram.0[25] = and.length_b.into();

    ram
}

#[test]
fn and_gate_true() {
    let mut ram = setup_and();

    // Set both conditions as true
    ram.0[22] = Word::Data(Data::Length(2));
    ram.0[23] = Word::Data(Data::Length(2));

    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("true"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}

#[test]
fn and_gate_false_a() {
    let mut ram = setup_and();

    // Set a condition as true
    ram.0[22] = Word::Data(Data::Length(2));

    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("false"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}

#[test]
fn and_gate_false_b() {
    let mut ram = setup_and();

    // Set a condition as true
    ram.0[23] = Word::Data(Data::Length(2));

    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("false"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}

#[test]
fn and_gate_false_ab() {
    let mut ram = setup_and();
    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("false"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}

fn setup_or() -> Memory {
    let mut ram = Memory::new(26);

    let or = Or::new(
        // Scratchspace
        16,

        // Boolean conditions
        1, 22, 3,
        4, 23, 6,

        // Load & jump
        7, 17,
        8,

        // True and false branches
        20, 21,
    );

    // Set false to entire scratch
    ram.0[0] = or.spray.into();

    // Evaluate A
    ram.0[1] = or.length_a.into();
    ram.0[2] = or.length_a_ptr.into();
    ram.0[3] = or.a.into();

    // Evaluate B
    ram.0[4] = or.length_b.into();
    ram.0[5] = or.length_b_ptr.into();
    ram.0[6] = or.b.into();

    // Jump to result
    ram.0[7] = or.load.into();
    ram.0[8] = or.jmp.into();

    ram.0[9] = or.true_ptr.into();
    ram.0[10] = or.false_ptr.into();

    // Logic scratchpad
    ram.0[16] = Word::Reserved;
    ram.0[17] = Word::Reserved;

    // Setup branch targets
    ram.0[20] = Word::Protected("true");
    ram.0[21] = Word::Protected("false");

    // Constant true and false values
    ram.0[22] = Word::Data(Data::Length(1)); // 2 = true, 1 = false
    ram.0[23] = Word::Data(Data::Length(1)); // as above

    ram
}

#[test]
fn or_gate_false() {
    let mut ram = setup_and();
    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("false"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}

#[test]
fn or_gate_a() {
    let mut ram = setup_or();
    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    // Set a condition as true
    ram.0[22] = Word::Data(Data::Length(2));

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("true"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}

#[test]
fn or_gate_b() {
    let mut ram = setup_or();
    let mut vm = Vm::try_new(16, 0, &ram).unwrap();

    // Set a condition as true
    ram.0[23] = Word::Data(Data::Length(2));

    let as_expected = match vm.run(&mut ram) {
        Ok(h) => panic!("program should have failed, histoy: {:?}", h),
        Err(e) => match e {
            (VmError::InvalidDescriptor(d), _) => d == Word::Protected("true"),
            _ => false,
        },
    };

    assert!(as_expected, "the program did not return the expected values, instead it got {}", as_expected);
}
