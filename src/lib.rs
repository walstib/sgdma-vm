//! SGDMA Virtual Machine
//!
//! Loosely modeled after the PrimeCell (PL172)
pub mod logic;
pub mod symbol_table;
pub mod vm;
mod test;

pub use logic::*;
pub use symbol_table::*;
pub use vm::*;

/// Run the supplied DMA program and print statistics
pub fn run(vm: &mut Vm, ram: &mut Memory) {
    // Count total number of unused reservations
    let mut num_reserved = 0;
    for w in ram.0.iter() {
        if let Word::Reserved = w {
            num_reserved += 1;
        }
    }

    match vm.run(ram) {
        Ok(dl) => {
            println!("The program ended succesfully");
            for (i, d) in dl.iter().enumerate() {
                println!(" {: >#4}: {:?}", i, d);
            }
        }
        Err((e, wl)) => {
            println!("The program failed with: {:?}", e);
            for (i, d) in wl.iter().enumerate() {
                println!("{: >4}: {:?}", i, d);
            }
        }
    }

    println!("Memory:\r\n{}", ram);

    // Count number of unused reservation
    let mut num_unused = 0;
    for w in ram.0.iter() {
        if let Word::Reserved = w {
            num_unused += 1;
        }
    }

    // Show usage of reserved space
    let left = num_reserved - num_unused;
    let percentage = left as f32 / num_reserved as f32 * 100.0;
    println!("Reserve usage: {:.2}% ({}/{})", percentage, left, num_reserved);
}
