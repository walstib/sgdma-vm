//! Simple arithmetic example
use sgdma_vm::*;

fn lut<F: Fn(usize) -> usize>(range: core::ops::Range<usize>, f: F) -> Vec<Word> {
    let mut output = Vec::new();

    for x in range {
        output.push(Word::Data(Data::Length(f(x))));
    }

    output
}

fn main() {
    let mut ram = Memory::new(272);
    let c = Counter::new(1, true);

    // Copy transfer template and jump template
    let setup = Descriptor {
        source: 0x06,
        dest: 0x01,
        len: 4,
        next: Some(1),
        increment_source: true,
        increment_dest: true,
    };

    // Output a new number to calculate on
    let copy = Descriptor {
        source: 0x0a,
        dest: 0x0b,
        len: 1,
        next: Some(2),
        increment_source: false,
        increment_dest: false,
    };

    // Setup length parameter
    let length = Descriptor {
        source: 0x0b,
        dest: 0x03, // Address of apply_lut
        len: 1,
        next: Some(0x03), // Address of apply_lut
        increment_source: false,
        increment_dest: false,
    };

    // Transform length into output
    let apply_lut = Descriptor {
        source: 0x10, // Address of wrapping_add
        dest: 0xc, // Output will be the computed value
        len: 0, // Setup by length
        next: Some(0x04),
        increment_source: true,
        increment_dest: false,
    };

    // Copy the setup template and jump back to it
    let jump = Descriptor {
        source: 0x05,
        dest: 0x00,
        len: 1,
        next: Some(0),
        increment_source: false,
        increment_dest: false,
    };

    // Construct a lookup table for addition
    let wrapping_add = lut(0..256, |x| {x.wrapping_add(5) % 256});

    // Initial transfer setup
    ram.0[0x00] = setup.into();
    ram.0[0x01] = copy.into();
    ram.0[0x02] = length.into();
    ram.0[0x03] = apply_lut.into();
    ram.0[0x04] = jump.into();

    // Template storage
    ram.0[0x05] = setup.into();
    ram.0[0x06] = copy.into();
    ram.0[0x07] = length.into();
    ram.0[0x08] = apply_lut.into();
    ram.0[0x09] = jump.into();

    // Counter
    ram.0[0x0a] = Word::Peripheral(c);

    // Counter output
    ram.0[0x0b] = Word::Reserved;

    // LUT output
    ram.0[0x0c] = Word::Reserved;

    // Setup addition LUT
    for (i, y) in wrapping_add.iter().enumerate() {
        ram.0[0x10 + i] = *y;
    }

    let mut vm = Vm::try_new(28, 0, &ram).unwrap();
    run(&mut vm, &mut ram);
}
