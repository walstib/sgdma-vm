//! Perform cyclic conversions using templates and a DMA loop
use sgdma_vm::*;

fn main() {
    let mut ram = Memory::new(8);
    let c = Counter::new(0, true);

    // Copy transfer template and jump template
    let setup = Descriptor {
        source: 4,
        dest: 1,
        len: 2,
        next: Some(1),
        increment_source: true,
        increment_dest: true,
    };

    // Transfer 16 bytes out of the counter peripheral
    let copy = Descriptor {
        source: 6,
        dest: 7,
        len: 16,
        next: Some(2),
        increment_source: false,
        increment_dest: false,
    };

    // Copy the setup template and jump back to it
    let jump = Descriptor {
        source: 3,
        dest: 0,
        len: 1,
        next: Some(0),
        increment_source: false,
        increment_dest: false,
    };

    // Initial transfer setup
    ram.0[0] = setup.into();
    ram.0[1] = copy.into();
    ram.0[2] = jump.into();

    // Template storage
    ram.0[3] = setup.into();
    ram.0[4] = copy.into();
    ram.0[5] = jump.into();

    // Mapped memory
    ram.0[6] = Word::Peripheral(c);

    // Output memory
    ram.0[7] = Word::Reserved;

    let mut vm = Vm::try_new(255, 0, &ram).unwrap();
    run(&mut vm, &mut ram);
}
