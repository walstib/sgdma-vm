//! Implementation of FizzBuzz using multiple LUTs and an AND gate
use sgdma_vm::*;

fn lut<F: Fn(usize) -> usize>(range: core::ops::Range<usize>, f: F) -> Vec<Word> {
    let mut output = Vec::new();

    for x in range {
        output.push(Word::Data(Data::Length(f(x))));
    }

    output
}

/// Prepare symbol table
fn link<const SIZE: usize>() -> Result<SymbolTable<SIZE>, VmError> {
    let mut sym_table = SymbolTable::new();

    // Descriptor storage
    sym_table.assign(0x00, "setup")?;
    sym_table.assign(0x01, "copy")?;
    sym_table.assign(0x02, "length_mod3")?;
    sym_table.assign(0x03, "apply_mod3_lut")?;
    sym_table.assign(0x04, "length_mod5")?;
    sym_table.assign(0x05, "apply_mod5_lut")?;
    sym_table.assign(0x06, "length_mod15")?;
    sym_table.assign(0x07, "apply_mod15_lut")?;
    sym_table.assign(0x08, "jump")?;

    // Template storage
    sym_table.assign(0x10, "t_setup")?;
    sym_table.assign(0x11, "t_copy")?;
    sym_table.assign(0x12, "t_length_mod3")?;
    sym_table.assign(0x13, "t_apply_mod3_lut")?;
    sym_table.assign(0x14, "t_length_mod5")?;
    sym_table.assign(0x15, "t_apply_mod5_lut")?;
    sym_table.assign(0x16, "t_length_mod15")?;
    sym_table.assign(0x17, "t_apply_mod15_lut")?;
    sym_table.assign(0x18, "t_jump")?;

    // Static values
    sym_table.assign(0x19, "prot_fizz")?;
    sym_table.assign(0x1a, "prot_buzz")?;
    sym_table.assign(0x1b, "prot_fizzbuzz")?;

    // Counter
    sym_table.assign(0x20, "counter")?;

    // Counter output
    sym_table.assign(0x21, "counter_out")?;

    // LUT output
    sym_table.assign(0x22, "mod3_out")?;
    sym_table.assign(0x23, "mod5_out")?;
    sym_table.assign(0x24, "mod15_out")?;

    // LUT storage
    sym_table.assign(0x30, "mod3_lut")?;
    sym_table.assign(0x130, "mod5_lut")?;
    sym_table.assign(0x230, "mod15_lut")?;

    Ok(sym_table)
}

fn run_main(time_limit: usize) -> Result<(), VmError> {
    let st: SymbolTable<0x330> = link()?;
    let mut ram = Memory::new(0x330);
    let c = Counter::new(1, true);

    // Copy transfer templates
    let setup = Descriptor {
        source: st.get("t_copy")?,
        dest: st.get("copy")?,
        len: 0xf,
        next: Some(st.get("copy")?),
        increment_source: true,
        increment_dest: true,
    };

    // Output a new number to calculate on
    let copy = Descriptor {
        source: st.get("counter")?,
        dest: st.get("counter_out")?,
        len: 1,
        next: Some(st.get("length_mod3")?),
        increment_source: false,
        increment_dest: false,
    };

    // Setup length parameter for LUT
    let length_mod3 = Descriptor {
        source: st.get("counter_out")?,
        dest: st.get("apply_mod3_lut")?, // Address of apply_lut
        len: 1,
        next: Some(st.get("apply_mod3_lut")?), // Address of apply_lut
        increment_source: false,
        increment_dest: false,
    };

    // Transform length into output
    let apply_mod3_lut = Descriptor {
        source: st.get("mod3_lut")?, // Address of lut
        dest: st.get("mod3_out")?, // Output will be the computed value
        len: 0, // Setup by length
        next: Some(st.get("length_mod5")?),
        increment_source: true,
        increment_dest: false,
    };

    // Setup length parameter for LUT
    let length_mod5 = Descriptor {
        source: st.get("counter_out")?,
        dest: st.get("apply_mod5_lut")?, // Address of apply_lut
        len: 1,
        next: Some(st.get("apply_mod5_lut")?), // Address of apply_lut
        increment_source: false,
        increment_dest: false,
    };

    // Transform length into output
    let apply_mod5_lut = Descriptor {
        source: st.get("mod5_lut")?,
        dest: st.get("mod5_out")?,
        len: 0,
        next: Some(st.get("length_mod15")?),
        increment_source: true,
        increment_dest: false,
    };

    // Setup length parameter for LUT
    let length_mod15 = Descriptor {
        source: st.get("counter_out")?,
        dest: st.get("apply_mod15_lut")?,
        len: 1,
        next: Some(st.get("apply_mod15_lut")?),
        increment_source: false,
        increment_dest: false,
    };

    // Transform length into output
    let apply_mod15_lut = Descriptor {
        source: st.get("mod15_lut")?,
        dest: st.get("mod15_out")?,
        len: 0,
        next: Some(st.get("jump")?),
        increment_source: true,
        increment_dest: false,
    };

    // Copy the setup template and jump back to it
    let jump = Descriptor {
        source: st.get("t_setup")?,
        dest: st.get("setup")?,
        len: 1,
        next: Some(st.get("setup")?),
        increment_source: false,
        increment_dest: false,
    };

    // Construct lookup tables mapping between an input and it being evenly divisible
    let mod3 = lut(1..256, |x| { if x % 3 == 0 { 2 } else { 1 } });
    let mod5 = lut(1..256, |x| { if x % 5 == 0 { 2 } else { 1 } });
    let mod15 = lut(1..256, |x| { if x % 15 == 0 { 2 } else { 1 } });

    // Initial transfer setup
    ram.0[st.get("setup")?] = setup.into();
    ram.0[st.get("copy")?] = copy.into();
    ram.0[st.get("length_mod3")?] = length_mod3.into();
    ram.0[st.get("apply_mod3_lut")?] = apply_mod3_lut.into();
    ram.0[st.get("length_mod5")?] = length_mod5.into();
    ram.0[st.get("apply_mod5_lut")?] = apply_mod5_lut.into();
    ram.0[st.get("length_mod15")?] = length_mod15.into();
    ram.0[st.get("apply_mod15_lut")?] = apply_mod15_lut.into();
    ram.0[st.get("jump")?] = jump.into();

    // Template storage
    ram.0[st.get("t_setup")?] = setup.into();
    ram.0[st.get("t_copy")?] = copy.into();
    ram.0[st.get("t_length_mod3")?] = length_mod3.into();
    ram.0[st.get("t_apply_mod3_lut")?] = apply_mod3_lut.into();
    ram.0[st.get("t_length_mod5")?] = length_mod5.into();
    ram.0[st.get("t_apply_mod5_lut")?] = apply_mod5_lut.into();
    ram.0[st.get("t_length_mod15")?] = length_mod15.into();
    ram.0[st.get("t_apply_mod15_lut")?] = apply_mod15_lut.into();
    ram.0[st.get("t_jump")?] = jump.into();

    // Static values
    ram.0[st.get("prot_fizz")?] = Word::Protected("Fizz");
    ram.0[st.get("prot_buzz")?] = Word::Protected("Buzz");
    ram.0[st.get("prot_fizzbuzz")?] = Word::Protected("FizzBuzz");

    // Counter
    ram.0[st.get("counter")?] = Word::Peripheral(c);

    // Counter output
    ram.0[st.get("counter_out")?] = Word::Reserved;

    // LUT output
    ram.0[st.get("mod3_out")?] = Word::Reserved;
    ram.0[st.get("mod5_out")?] = Word::Reserved;
    ram.0[st.get("mod15_out")?] = Word::Reserved;

    // Setup modulo LUTs
    for (i, y) in mod3.iter().enumerate() {
        ram.0[st.get("mod3_lut")? + i] = *y;
    }

    for (i, y) in mod5.iter().enumerate() {
        ram.0[st.get("mod5_lut")? + i] = *y;
    }

    for (i, y) in mod15.iter().enumerate() {
        ram.0[st.get("mod15_lut")? + i] = *y;
    }

    st.check_init(&ram)?;
    let mut vm = Vm::try_new(time_limit, 0, &ram).unwrap();
    run(&mut vm, &mut ram);

    Ok(())
}

fn main() {
    run_main(512 + 128 + 18).unwrap();
}
