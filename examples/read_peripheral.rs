//! Read data from a peripheral
use sgdma_vm::*;

fn main() {
    let mut ram = Memory::new(3);
    let c = Counter::new(0, true);
    let copy = Descriptor {
        source: 0,
        dest: 1,
        len: 16,
        next: None,
        increment_source: false,
        increment_dest: false,
    };

    ram.0[0] = Word::Peripheral(c);
    ram.0[1] = Word::Reserved;
    ram.0[2] = copy.into();

    let mut vm = Vm::try_new(24, 2, &ram).unwrap();
    run(&mut vm, &mut ram);

    assert!(if let Word::Data(Data::Byte(15)) = ram.0[1] { true } else { false });
}
