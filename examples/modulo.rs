//! Implementation of multiple LUTs
use sgdma_vm::*;

fn lut<F: Fn(usize) -> usize>(range: core::ops::Range<usize>, f: F) -> Vec<Word> {
    let mut output = Vec::new();

    for x in range {
        output.push(Word::Data(Data::Length(f(x))));
    }

    output
}

fn main() {
    let mut ram = Memory::new(0x230);
    let c = Counter::new(1, true);

    // Copy transfer templates
    let setup = Descriptor {
        source: 0x11,
        dest: 0x01,
        len: 7,
        next: Some(0x01),
        increment_source: true,
        increment_dest: true,
    };

    // Output a new number to calculate on
    let copy = Descriptor {
        source: 0x20,
        dest: 0x21,
        len: 1,
        next: Some(0x02),
        increment_source: false,
        increment_dest: false,
    };

    // Setup length parameter for LUT
    let length_mod3 = Descriptor {
        source: 0x21,
        dest: 0x03, // Address of apply_lut
        len: 1,
        next: Some(0x03), // Address of apply_lut
        increment_source: false,
        increment_dest: false,
    };

    // Transform length into output
    let apply_mod3_lut = Descriptor {
        source: 0x30, // Address of lut
        dest: 0x22, // Output will be the computed value
        len: 0, // Setup by length
        next: Some(0x04),
        increment_source: true,
        increment_dest: false,
    };

    // Setup length parameter for LUT
    let length_mod5 = Descriptor {
        source: 0x21,
        dest: 0x05, // Address of apply_lut
        len: 1,
        next: Some(0x05), // Address of apply_lut
        increment_source: false,
        increment_dest: false,
    };

    // Transform length into output
    let apply_mod5_lut = Descriptor {
        source: 0x130,
        dest: 0x23, // Output will be the computed value
        len: 0, // Setup by length
        next: Some(0x06),
        increment_source: true,
        increment_dest: false,
    };

    // Copy the setup template and jump back to it
    let jump = Descriptor {
        source: 0x10,
        dest: 0x00,
        len: 1,
        next: Some(0x00),
        increment_source: false,
        increment_dest: false,
    };

    // Construct lookup tables mapping between an input and it being evenly divisible
    let mod3 = lut(0..256, |x| { if x % 3 == 0 { 2 } else { 1 } });
    let mod5 = lut(0..256, |x| { if x % 5 == 0 { 2 } else { 1 } });

    // Initial transfer setup
    ram.0[0x00] = setup.into();
    ram.0[0x01] = copy.into();
    ram.0[0x02] = length_mod3.into();
    ram.0[0x03] = apply_mod3_lut.into();
    ram.0[0x04] = length_mod5.into();
    ram.0[0x05] = apply_mod5_lut.into();
    ram.0[0x06] = jump.into();

    // Template storage
    ram.0[0x10] = setup.into();
    ram.0[0x11] = copy.into();
    ram.0[0x12] = length_mod3.into();
    ram.0[0x13] = apply_mod3_lut.into();
    ram.0[0x14] = length_mod5.into();
    ram.0[0x15] = apply_mod5_lut.into();
    ram.0[0x16] = jump.into();

    // Counter
    ram.0[0x20] = Word::Peripheral(c);

    // Counter output
    ram.0[0x21] = Word::Reserved;

    // LUT output
    ram.0[0x22] = Word::Reserved;
    ram.0[0x23] = Word::Reserved;
    ram.0[0x24] = Word::Reserved;

    // Setup modulo LUTs
    for (i, y) in mod3.iter().enumerate() {
        ram.0[0x30 + i] = *y;
    }

    for (i, y) in mod5.iter().enumerate() {
        ram.0[0x130 + i] = *y;
    }

    let mut vm = Vm::try_new(64, 0, &ram).unwrap();
    run(&mut vm, &mut ram);
}
